import 'package:flutter/material.dart';
import 'package:responsividade/desktop_scaffold.dart';
import 'package:responsividade/mobile_scaffold.dart';
import 'package:responsividade/responsive_layout.dart';
import 'package:responsividade/tablet_scaffold.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ResponsiveLayout(
          mobileScaffold: MobileScaffold(),
          tabletScaffold: TabletScaffold(),
          desktopScaffold: DesktopScaffold()),
    );
  }
}
