import 'package:flutter/material.dart';

var myDefaultBackgroundCollor = Colors.grey[300];

var myAppbar = AppBar(
  backgroundColor: Colors.grey[900],
);

var myDrawer = Drawer(
  backgroundColor: myDefaultBackgroundCollor,
  child: const Column(
    children: [
      DrawerHeader(
        child: Icon(Icons.favorite),
      ),
      ListTile(
        leading: Icon(Icons.chat),
        title: Text('M E S S A G E  '),
      ),
      ListTile(
        leading: Icon(Icons.settings),
        title: Text('S E T T I N G S  '),
      ),
      ListTile(
        leading: Icon(Icons.logout),
        title: Text('L O G O U T  '),
      ),
    ],
  ),
);
